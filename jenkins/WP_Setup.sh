#!/bin/bash

#add key and active ssh agent, cloning wp_teststand or git pull if wp_teststand exist
echo Checking wp_teststand in synergo directory
if [ -d /var/lib/jenkins/synergo/wp_teststand ]; then
  echo wp_teststand found checking for update
  cd /var/lib/jenkins/synergo/wp_teststand
  ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git pull"
else
  echo can not found wp_teststand in echo Checking wp_teststand in synergo directory clonning from repositories
  ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git clone git@bitbucket.org:synergoteam/wp_teststand.git $HOME/synergo/wp_teststand"
fi
cp -ar /var/lib/jenkins/synergo/wp_teststand $BUILDENV

#Setup database
export db_app_version=${DB_NAME}_${app_version_build//./_}
mysql -u${DB_USER} -p${DB_PASSWORD} -h${DB_HOST} -e "DROP DATABASE IF EXISTS \`${db_app_version}\`; CREATE DATABASE \`${db_app_version}\`;"

#download wordpress using wp-cli
if ! type "wp-cli" >/dev/null 2>&1; then 
  echo wp-cli command not found; 
  exit 1
else
  wp-cli core download --version=$app_version_build --path=$BUILDENV/htdocs
fi

#git clone parent repo if parent_git_url variable is specified
if [ ! -z $parent_git_url ]; then
 export parent_dir=$BUILDENV/$builddir/`basename $parent_git_url .git`/
 export parent_cache=/var/lib/jenkins/synergo/`basename $parent_git_url .git`

 #clone parent theme to synergo folder
 if [ -d $parent_cache ]; then
   if [ "$(ls -A $parent_cache)" ]; then
     cd $parent_cache
     ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git pull"
   else
     ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git clone $parent_git_url $parent_cache"
   fi
 else
     ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git clone $parent_git_url $parent_cache"
 fi
 #clone from cache to BUILDENV
 git clone $parent_cache $parent_dir

 #git checkout to specific tag if $parent_git_tag is specified
 if [ ! -z $parent_git_tag ]; then
   cd $parent_dir
   git checkout $parent_git_tag
 fi
else
  export parent_dir=$BUILDENV/$builddir/$workspace_dir/
fi

#unzip parent plugins
if [ ! -z $parent_plugin_path ]; then
echo -e "parent pugins found \n";
unzip -q "$parent_dir/$parent_plugin_path/*.zip" -d $BUILDENV/htdocs/wp-content/plugins/ 
fi


#unzip child plugins
if [ ! -z $child_plugin_path ]; then
echo -e "child pugins found \n";
unzip -q "$proj_dir/$child_plugin_path/*.zip" -d $BUILDENV/htdocs/wp-content/plugins/
fi

#running phpunit 
cd /${BUILDENV}/wp_teststand
~/workspace/vendor/bin/phpunit --verbose --coverage-html $WORKSPACE/build/coverage --coverage-clover $WORKSPACE/build/logs/clover.xml --coverage-crap4j $WORKSPACE/build/logs/crap4j.xml  --log-junit $WORKSPACE/build/logs/junit.xml --test-suffix .php $proj_dir/tests
