#!/bin/bash

die() { echo "$*" >&2; usage; exit 2; }

chck_cmmnd()
{
if command -v $1 >/dev/null 2>&1; then
  return 0
else
  echo $1 command not found
  exit 1
fi
}

chck_null()
{
if [ -z $[$1] ]; then
  echo variable $1 is null;
  exit 1
else
  return true
fi
}

# cloning parent and child repo
clone_folder()
{
if [ -d /var/www/$doc_root/$target_dir/$1 ]; then
    if [ -d /var/www/$doc_root/$target_dir/$1/.git ]; then
    echo -e "\nexisting $4 module found"
    echo checking for update
    cd /var/www/$doc_root/$target_dir/$1/
     git checkout master  >/dev/null 2>&1; git pull;
    if [ -z $4 ]; then
      git checkout $3 >/dev/null 2>&1
      echo -e "\nchecking out to commit $3"
    else
      if [ ! -z $parent_git_commit ]; then
        git checkout $parent_git_commit >/dev/null 2>&1
        echo -e "\nchecking out to commit $parent_git_commit"
      fi
    fi
  else
    rm -rf /var/www/$doc_root/$target_dir/$1
    git clone $2 /var/www/$doc_root/$target_dir/$1
    cd /var/www/$doc_root/$target_dir/$1/
    if [ -z $4 ]; then
      git checkout $3 >/dev/null 2>&1
      echo -e "\nchecking out to commit $3"
    else
      if [ ! -z $parent_git_commit ]; then
        git checkout $parent_git_commit >/dev/null 2>&1
        echo -e "\nchecking out to commit $parent_git_commit"
      fi
    fi
  fi
else
   echo -e "\nno existing $4 module found creating directory /var/www/$doc_root/$target_dir/$1"
   git clone $2 /var/www/$doc_root/$target_dir/$1
   cd /var/www/$doc_root/$target_dir/$1/
   if [ -z $4 ]; then
     git checkout $3 >/dev/null 2>&1
     echo -e "\nchecking out to commit $3"
   else
      if [ ! -z $parent_git_commit ]; then
        git checkout $parent_git_commit >/dev/null 2>&1
        echo -e "\nchecking out to commit $parent_git_commit"
      fi

   fi
fi
}

usage()
{
cat << EOF
usage: $0 options

This script run for deploying from jenkins workspace to target server directory.

OPTIONS:
   --private-key-path         specify a private key for cloning a project (default: $HOME/.ssh/deployment)
   --app-type                 specify framework type (Ex: magento)
   --app-version              APP version for framework installation
   --doc-root                 specify module project root
   --target-dir               spesify target dir where module wil be clone
   --git-url                  specify module git url
   --git-commit               specify git commit where module must checkout
   --parent-git-url           specify parent module git url if cloned module have parent module
   --parent-git-tag        specify parent module commit, leave it blank if not use specific parent git commit
EOF
}


app_type=""
private_key_path="$HOME/.ssh/deployment"

while getopts ab:c-: arg; do
  case $arg in
    - )  LONG_OPTARG="${OPTARG#*=}"
         case $OPTARG in
           app-version=?* )      app_version=$LONG_OPTARG ;;
           app-version* )        die "Option '--$OPTARG' requires an argument" ;;
           target-dir=?* )       target_dir=$LONG_OPTARG ;;
           target-dir* )         die "Option '--$OPTARG' requires an argument" ;;
           git-commit=?* )       git_commit=$LONG_OPTARG ;;
           git-commit* )         die "Option '--$OPTARG' requires an argument" ;;
           app-type=?* )         app_type=$LONG_OPTARG ;;
           app-type* )           die "Option '--$OPTARG' requires an argument" ;;
           doc-root=?* )         doc_root=$LONG_OPTARG ;;
           doc-root* )           die "Option '--$OPTARG' requires an argument" ;;
           git-url=?* )          git_url=$LONG_OPTARG ;;
           git-url* )            die "Option '--$OPTARG' requires an argument" ;;
           parent-git-url=?* )   parent_git_url=$LONG_OPTARG ;;
           parent-git-url* )     die "Option '--$OPTARG' requires an argument" ;;
           parent-git-tag=?* )parent_git_commit=$LONG_OPTARG ;;
           private-key-path=?* ) private_key_path=$LONG_OPTARG ;;
           private-key-path* )   die "Option '--$OPTARG' requires an argument" ;;
           help )                usage;;
           '' )           break ;; # "--" terminates argument processing
           * )            die "Illegal option --$OPTARG" ;;
         esac ;;
    \? ) exit 2 ;;
  esac
done

if  [ -z $app_version ]; then
echo app version is required, please specify app version with --app-version
exit 1
fi
if [ -z $target-dir ]; then
echo target dir is required, please specify app version with --target-dir
exit 1
fi
if [ -z $git-commit ]; then
echo git-commit is required, please specify app version with --git-commit
exit 1
fi
if [ -z $doc-root ]; then
echo doc-root is required, please specify app version with --doc-root
exit 1
fi
if [ -z $git_url ]; then
echo git_url is required, please specify app version with --git-url
exit 1
fi



if chck_cmmnd wp-cli; then
  #download wordpress to target directory
  if [ "$app_type" == "wordpress" ]; then
    if [ ! -f /var/www/$doc_root/index.php ]; then
      wp-cli core download --version=$app_version --path=/var/www/$doc_root
    else
      echo wordpress already installed
      cd /var/www/$doc_root/
      if [ ! `wp-cli core version` ==  $app_version ]; then
        echo '[WARNING]' installed version different with request version
        echo '[WARNING]' installed wordpress version = `wp-cli core version`
        echo '[WARNING]' request version             = $app_version
      fi
    fi
  #download magento to target directory
  elif [ "$app_type" == "magento" ]; then
    #insert how to download magento code here !!!
    echo no code
    exit 1;
  fi
fi


project_folder=`basename $git_url .git`

eval `ssh-agent -s`
ssh-add $private_key_path

clone_folder $project_folder $git_url $git_commit

if [ ! -z $parent_git_url ]; then
   parent_folder=`basename $parent_git_url .git`
   clone_folder $parent_folder $parent_git_url "parent_commit" "parent"
fi
